# Kamiko Save Tool

A tool for working with Kamiko (PC) save files.
Most of this tool is still highly experimental.

[Download latest version](https://gitlab.com/henny022/kamiko-save-tool/-/jobs/artifacts/master/download?job=package)

## Usage
This needs java to run.
everything version 1.8 or higher should work.
tested with 9 and 11.

This can be used as a gui or command line tool.

extract a save with
```
java -jar kamikosgt.jar read -s SaveData -t filename.json -n <save number> -j <character>
```
inject a save with
```
java -jar kamikosgt.jar write -s SaveData -t filename.json -n <save number>
```

## Thanks
Thanks to monkeyman192 for his python script (https://github.com/monkeyman192/KamikoSaveEditor).
Without the info on how the savefile works that I got from his script, not sure if I had made it this far.

Uses https://github.com/remkop/picocli for command line parsing
