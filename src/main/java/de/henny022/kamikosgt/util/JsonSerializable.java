package de.henny022.kamikosgt.util;

import de.henny022.kamikosgt.Main;

import java.io.*;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class JsonSerializable<T extends JsonSerializable>
{
    public void dumpJson(File file)
    {
        try (Writer writer = new FileWriter(file))
        {
            Main.prettyGson.toJson(this, writer);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public T loadJson(File file)
    {
        try (Reader reader = new FileReader(file))
        {
            return (T) Main.gson.fromJson(reader, getClass());
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException("invalid file", e);
        }
    }
}
