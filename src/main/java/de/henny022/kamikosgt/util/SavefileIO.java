package de.henny022.kamikosgt.util;

import de.henny022.kamikosgt.Main;
import de.henny022.kamikosgt.model.SaveFile;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Base64;
import java.util.MissingFormatArgumentException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * kamiko-save-tool, Created by henny on 07.07.2020.
 */
public class SavefileIO
{
    public static SaveFile read(File file) throws IOException
    {
        InputStream fileInput = new FileInputStream(file);
        InputStream decodedInput = Base64.getDecoder().wrap(fileInput);
        if(decodedInput.skip(4) != 4)
        {
            throw new IllegalArgumentException("file too small");
        }
        InputStream uncompressedInput = new GZIPInputStream(decodedInput);
        Reader reader = new InputStreamReader(uncompressedInput);
        StupidSaveFile stupidSaveFile = Main.prettyGson.fromJson(reader, StupidSaveFile.class);
        return stupidSaveFile.getSaveFile();
    }

    public static void write(File file, SaveFile saveFile)
    {
        String saveString = Main.gson.toJson(StupidSaveFile.fromSaveFile(saveFile));
        System.out.println(Integer.toHexString(saveString.length()));
        byte[] size = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(saveString.length()).array();
        try (OutputStream fileOutput = new FileOutputStream(file))
        {
            try (OutputStream encodedOutput = Base64.getEncoder().wrap(fileOutput))
            {
                encodedOutput.write(size);
                try (OutputStream compressedOutput = new KamikoOutputStream(encodedOutput))
                {
                    try (Writer writer = new OutputStreamWriter(compressedOutput))
                    {
                        writer.write(saveString);
                    }
                }
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
