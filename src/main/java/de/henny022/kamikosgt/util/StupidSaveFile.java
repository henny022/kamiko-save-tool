package de.henny022.kamikosgt.util;

import de.henny022.kamikosgt.Main;
import de.henny022.kamikosgt.model.MainSaveData;
import de.henny022.kamikosgt.model.SaveFile;
import de.henny022.kamikosgt.model.Settings;

/**
 * kamiko-save-tool, Created by henny on 07.07.2020.
 * no, I'm not toxic about this unnecessary extra decoding step
 */
public class StupidSaveFile
{
    public String MainSaveDataJson;
    public String SettingSaveDataJson;

    public static StupidSaveFile fromSaveFile(SaveFile saveFile)
    {
        StupidSaveFile stupidSaveFile = new StupidSaveFile();
        stupidSaveFile.MainSaveDataJson = Main.gson.toJson(saveFile.MainSaveDataJson);
        stupidSaveFile.SettingSaveDataJson = Main.gson.toJson(saveFile.SettingSaveDataJson);
        return stupidSaveFile;
    }

    public SaveFile getSaveFile()
    {
        SaveFile saveFile = new SaveFile();
        saveFile.MainSaveDataJson = Main.prettyGson.fromJson(MainSaveDataJson, MainSaveData.class);
        saveFile.SettingSaveDataJson = Main.prettyGson.fromJson(SettingSaveDataJson, Settings.class);
        return saveFile;
    }
}
