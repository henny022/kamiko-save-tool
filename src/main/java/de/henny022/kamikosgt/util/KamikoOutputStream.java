package de.henny022.kamikosgt.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.GZIPOutputStream;

/**
 * kamiko-save-tool, Created by henny on 07.07.2020.
 */
public class KamikoOutputStream extends GZIPOutputStream
{

    public KamikoOutputStream(OutputStream out) throws IOException
    {
        super(out);
        def.setLevel(6);
    }

    /*
     * Writes GZIP member header.
     * I hate Java
     * TODO somehow somewhere fix that 0x0a byte
     */
    private void writeHeader() throws IOException {
        out.write(new byte[] {
                (byte) 0x1f,              // Magic number (short)
                (byte) 0x8b,              // Magic number (short)
                Deflater.DEFLATED,        // Compression method (CM)
                0,                        // Flags (FLG)
                0,                        // Modification time MTIME (int)
                0,                        // Modification time MTIME (int)
                0,                        // Modification time MTIME (int)
                0,                        // Modification time MTIME (int)
                0,                        // Extra flags (XFLG)
                (byte) 0x0a               // Operating system (OS)
        });
    }
}
