package de.henny022.kamikosgt.model;

import java.util.List;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class MapSaveDataUnit
{
    public String _mapKey;
    public List<EnemyArea> _enemyAreaSaveDatalist;
    public List<MapObject> _mapObjectSaveInfo;
    public List<HPObject> _recoverHPObjectInfoList;
    public int _creatingRecoveryHPObjectNum;
    public int _brokenBushNum;
    public List<IntPosition> _deadPlayerPositionList;
    public List<IntPosition> _deadPlayerInFinalBossPositionList;
}
