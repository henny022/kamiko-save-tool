package de.henny022.kamikosgt.model;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class Character
{
    public static final Character YAMATO = new Character("Yamato", 0, "SwordPlayer");
    public static final Character UZUME = new Character("Uzume", 1, "BowPlayer");
    public static final Character HINOME = new Character("Hinome", 2, "ShieldPlayer");

    public String name;
    public int job;
    public String key;

    private Character(String name, int job, String key)
    {
        this.name = name;
        this.job = job;
        this.key = key;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
