package de.henny022.kamikosgt.model;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class PlayerData
{
    public Status _playerStatus;
    public int _currentPlayerJob;
    public DoublePosition _position;
    public int _direction;
    public boolean _isWithTorii;
    public boolean _isBeforeBoss;
}
