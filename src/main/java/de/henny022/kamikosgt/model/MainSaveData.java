package de.henny022.kamikosgt.model;

import java.util.List;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class MainSaveData
{
    public int _saveDataNo;
    public List<SaveGame> _saveDataUnitList;

    public void replaceSaveGame(SaveGame saveGame)
    {
        _saveDataUnitList.removeIf(unit -> unit._dataNo == saveGame._dataNo);
        _saveDataUnitList.add(saveGame);
    }

    public void replaceCharacterData(int saveGame, CharacterData characterData)
    {
        getSaveGame(saveGame).replaceCharacterData(characterData);
    }

    public SaveGame getSaveGame(int no)
    {
        return _saveDataUnitList.stream().filter(saveGame -> saveGame._dataNo == no).findFirst().get();
    }

    public CharacterData getCharacterData(int saveGame, int job)
    {
        return getSaveGame(saveGame).getCharacterData(job);
    }
}
