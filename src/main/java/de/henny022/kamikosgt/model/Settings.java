package de.henny022.kamikosgt.model;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class Settings
{
    public boolean _isInitialized;
    public boolean ShouldShowMapTime;
    public boolean ShouldShakeCamera;
    public int _currentLanguage;  //TODO make enum or something
    public boolean _isMuteSE;
    public boolean _isMuteBGM;
    public int _audioLevel;
    public int _dashKeyTypeNo;
}
