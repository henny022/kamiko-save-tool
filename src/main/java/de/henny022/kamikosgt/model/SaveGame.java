package de.henny022.kamikosgt.model;

import java.util.List;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class SaveGame
{
    /**
     * this save datas slot
     */
    public int _dataNo;
    /**
     * preselected job in the menu
     */
    public int _currentJob;
    /**
     * each characters save data
     */
    public List<CharacterData> _saveDataUnitEachJobList;
    /**
     * has a secret ever been collected (for the achievement)
     */
    public List<Boolean> IsGotHiddenItemList;
    /**
     * has the special attack been used with a character (for the achievement)
     */
    public List<Boolean> IsDoneAccumulateAttackActionList;
    /**
     * total enemies beaten (for the achievement)
     */
    public int _beatenEnemyCount;
    /**
     * total objects broken (for the achievement)
     */
    public int _brokenObjectCount;

    public void replaceCharacterData(CharacterData characterData)
    {
        _saveDataUnitEachJobList.removeIf(data -> data.getJob() == characterData.getJob());
        _saveDataUnitEachJobList.add(characterData);
    }

    public CharacterData getCharacterData(int job)
    {
        return _saveDataUnitEachJobList.stream().filter(characterData -> characterData.getJob() == job).findFirst().get();
    }
}
