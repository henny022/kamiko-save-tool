package de.henny022.kamikosgt.model;

import de.henny022.kamikosgt.util.JsonSerializable;

import java.util.List;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class CharacterData extends JsonSerializable<CharacterData>
{
    public int _playerJob;
    public PlayerData PlayerData;
    public MapData MapData;
    public boolean _isSawPrologue;
    public boolean _isCleared;
    public List<Double> _highScoreTimeList;
    public List<Integer> _clearTimeEachMapList;
    public int _totalPlayTime;
    public double _playTimeForMap;
    public boolean IsDoneContinue;
    public boolean IsDamaged;

    public int getJob()
    {
        return PlayerData._currentPlayerJob;
    }
}
