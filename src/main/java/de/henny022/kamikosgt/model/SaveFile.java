package de.henny022.kamikosgt.model;

import de.henny022.kamikosgt.util.JsonSerializable;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class SaveFile extends JsonSerializable<SaveFile>
{
    public MainSaveData MainSaveDataJson;
    public Settings SettingSaveDataJson;

    public void replaceSaveGame(SaveGame saveGame)
    {
        MainSaveDataJson.replaceSaveGame(saveGame);
    }

    public void replaceCharacterData(int saveGame, CharacterData characterData)
    {
        MainSaveDataJson.replaceCharacterData(saveGame, characterData);
    }

    public SaveGame getSaveGame(int no)
    {
        return MainSaveDataJson.getSaveGame(no);
    }

    public CharacterData getCharacterData(int saveGame, int job)
    {
        return MainSaveDataJson.getCharacterData(saveGame, job);
    }
}
