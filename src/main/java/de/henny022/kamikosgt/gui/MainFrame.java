package de.henny022.kamikosgt.gui;

import javax.swing.*;
import java.awt.*;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class MainFrame extends KamikoFrame
{
    public MainFrame() throws HeadlessException
    {
        super("Kamiko Save Tool");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setupComponents();
    }

    public static void launch()
    {
        MainFrame frame = new MainFrame();
        frame.setVisible(true);
    }

    private void setupComponents()
    {
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        JButton extractButton = new JButton("Read");
        extractButton.addActionListener(e -> ExtractionFrame.launch());
        getContentPane().add(extractButton);

        JButton injectButton = new JButton("Write");
        injectButton.addActionListener(e -> InjectionFrame.launch());
        getContentPane().add(injectButton);

        pack();
    }
}
