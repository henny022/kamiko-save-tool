package de.henny022.kamikosgt.gui;

import javax.swing.*;
import java.awt.*;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public abstract class KamikoFrame extends JFrame
{
    public KamikoFrame(String title) throws HeadlessException
    {
        super(title);
    }
}
