package de.henny022.kamikosgt.gui;

import de.henny022.kamikosgt.Main;
import de.henny022.kamikosgt.model.Character;
import de.henny022.kamikosgt.model.CharacterData;
import de.henny022.kamikosgt.model.SaveFile;
import de.henny022.kamikosgt.util.SavefileIO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class InjectionFrame extends KamikoFrame implements ActionListener
{
    RadioSelection<Integer> savefileSelection;
    FileSelection inFileSelection;
    FileSelection outFileSelection;

    public InjectionFrame() throws HeadlessException
    {
        super("Write Save");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setupComponents();
    }

    public static void launch()
    {
        InjectionFrame frame = new InjectionFrame();
        frame.setVisible(true);
    }

    private void setupComponents()
    {
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        savefileSelection = RadioSelection.create(getContentPane(), 0, 1, 2);

        inFileSelection = new FileSelection(new File(Main.KAMIKO_DIR + "SaveData"), new File(Main.KAMIKO_DIR));
        getContentPane().add(inFileSelection);

        outFileSelection = new FileSelection();
        getContentPane().add(outFileSelection);

        JButton extractButton = new JButton("Write");
        extractButton.addActionListener(this);
        getContentPane().add(extractButton);

        pack();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        int savefile = savefileSelection.getSelection();
        File infile = inFileSelection.getFile();
        File outfile = outFileSelection.getFile();

        try
        {
            SaveFile saveFile = SavefileIO.read(infile);
            CharacterData data = new CharacterData().loadJson(outfile);
            saveFile.replaceCharacterData(savefile, data);
            SavefileIO.write(infile, saveFile);
            JOptionPane.showMessageDialog(this, "ok");
        }
        catch (IOException ioException)
        {
            ioException.printStackTrace();
            JOptionPane.showMessageDialog(this, "error");
        }
    }
}
