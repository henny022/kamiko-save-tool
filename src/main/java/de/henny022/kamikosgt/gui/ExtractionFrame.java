package de.henny022.kamikosgt.gui;

import de.henny022.kamikosgt.Main;
import de.henny022.kamikosgt.model.Character;
import de.henny022.kamikosgt.model.CharacterData;
import de.henny022.kamikosgt.model.SaveFile;
import de.henny022.kamikosgt.util.SavefileIO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class ExtractionFrame extends KamikoFrame implements ActionListener
{
    RadioSelection<Integer> savefileSelection;
    RadioSelection<Character> characterSelection;
    FileSelection inFileSelection;
    FileSelection outFileSelection;

    public ExtractionFrame() throws HeadlessException
    {
        super("Read Save");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setupComponents();
    }

    public static void launch()
    {
        ExtractionFrame frame = new ExtractionFrame();
        frame.setVisible(true);
    }

    private void setupComponents()
    {
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        savefileSelection = RadioSelection.create(getContentPane(), 0, 1, 2);

        characterSelection = RadioSelection.create(getContentPane(), Character.YAMATO, Character.UZUME, Character.HINOME);

        inFileSelection = new FileSelection(new File(Main.KAMIKO_DIR + "SaveData"), new File(Main.KAMIKO_DIR));
        getContentPane().add(inFileSelection);

        outFileSelection = new FileSelection();
        getContentPane().add(outFileSelection);

        JButton extractButton = new JButton("Read");
        extractButton.addActionListener(this);
        getContentPane().add(extractButton);

        pack();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        int savefile = savefileSelection.getSelection();
        Character character = characterSelection.getSelection();
        File infile = inFileSelection.getFile();
        File outfile = outFileSelection.getFile();

        try
        {
            SaveFile saveFile = SavefileIO.read(infile);
            CharacterData data = saveFile.getCharacterData(savefile, character.job);
            data.dumpJson(outfile);
            JOptionPane.showMessageDialog(this, "ok");
        }
        catch (IOException ioException)
        {
            ioException.printStackTrace();
            JOptionPane.showMessageDialog(this, "error");
        }
    }
}
