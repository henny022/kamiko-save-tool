package de.henny022.kamikosgt.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class RadioSelection<T> implements ActionListener
{
    private T selection;
    private Map<String, T> actionMap;
    private ButtonGroup buttonGroup;

    public RadioSelection()
    {
        actionMap = new HashMap<>();
        buttonGroup = new ButtonGroup();
    }

    public static <T> RadioSelection<T> create(Container container, T... options)
    {
        RadioSelection<T> selection = new RadioSelection<>();
        for(T option : options)
        {
            JRadioButton button = new JRadioButton(option.toString());
            button.setActionCommand(option.toString());
            button.addActionListener(selection);
            selection.actionMap.put(option.toString(), option);
            selection.buttonGroup.add(button);
            container.add(button);
        }
        return selection;
    }

    public ButtonGroup getButtonGroup()
    {
        return buttonGroup;
    }

    public T getSelection()
    {
        return selection;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        selection = actionMap.get(e.getActionCommand());
    }
}
