package de.henny022.kamikosgt.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * kamiko-save-tool, Created by henny on 08.07.2020.
 */
public class FileSelection extends Container implements ActionListener
{
    private File file;
    private JFileChooser chooser;
    private JLabel label;

    public FileSelection()
    {
        this(null, new JFileChooser(), new File("."));
    }

    public FileSelection(File preselectedFile, File directory)
    {
        this(preselectedFile, new JFileChooser(), directory);
    }

    public FileSelection(File preselectedFile, JFileChooser fileChooser, File directory)
    {
        this.file = preselectedFile;
        this.chooser = fileChooser;
        chooser.setCurrentDirectory(directory);
        setupContent();
    }

    private void setupContent()
    {
        setLayout(new FlowLayout());
        label = new JLabel();
        if (file != null)
        {
            label.setText(file.getPath());
        }
        add(label);
        JButton button = new JButton("...");
        button.addActionListener(this);
        add(button);
    }

    public File getFile()
    {
        return file;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
        {
            file = chooser.getSelectedFile();
            label.setText(file.getPath());
        }
    }
}
