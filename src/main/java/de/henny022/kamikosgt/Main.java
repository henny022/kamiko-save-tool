package de.henny022.kamikosgt;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.henny022.kamikosgt.cli.CLI;
import de.henny022.kamikosgt.gui.MainFrame;
import picocli.CommandLine;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
public class Main
{
    public static final String KAMIKO_DIR = System.getenv("APPDATA") + "/../LocalLow/Flyhigh works/Kamiko/";
    // global state ...
    public static Gson prettyGson;
    public static Gson gson;

    public static void main(String[] args)
    {
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        prettyGson = builder.setPrettyPrinting().create();
        if (args.length == 0)
        {
            MainFrame.launch();
        }
        else
        {
            new CommandLine(new CLI()).execute(args);
        }
    }
}
