package de.henny022.kamikosgt.cli;

import de.henny022.kamikosgt.Main;
import de.henny022.kamikosgt.util.SavefileIO;
import de.henny022.kamikosgt.model.CharacterData;
import de.henny022.kamikosgt.model.SaveFile;
import picocli.CommandLine;

import java.io.*;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
@CommandLine.Command(name = "read")
public class Read implements Runnable
{
    @CommandLine.Option(names = {"-s", "--source"}, required = true)
    private File source;
    @CommandLine.Option(names = {"-t", "--target"}, required = true)
    private File target;
    @CommandLine.Option(names = {"-n", "--save"}, required = true)
    private int save;
    @CommandLine.Option(names = {"-j", "--job"}, required = true)
    private int job;

    @Override
    public void run()
    {
        try
        {
            SaveFile saveFile = SavefileIO.read(source);
            CharacterData data = saveFile.getCharacterData(save, job);
            data.dumpJson(target);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
