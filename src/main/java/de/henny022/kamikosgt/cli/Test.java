package de.henny022.kamikosgt.cli;

import de.henny022.kamikosgt.model.SaveFile;
import de.henny022.kamikosgt.util.SavefileIO;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;

/**
 * kamiko-save-tool, Created by henny on 07.07.2020.
 */
@CommandLine.Command(name = "test")
public class Test implements Runnable
{
    @CommandLine.Option(names = {"-s", "--source"}, required = true)
    private File source;
    @CommandLine.Option(names = {"-t", "--target"}, required = true)
    private File target;

    @Override
    public void run()
    {
        try
        {
            SaveFile saveFile = SavefileIO.read(source);
            SavefileIO.write(target, saveFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
