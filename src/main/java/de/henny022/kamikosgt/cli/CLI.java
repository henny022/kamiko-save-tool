package de.henny022.kamikosgt.cli;

import picocli.CommandLine;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
@CommandLine.Command(name = "kamiko-save-tool.jar", synopsisSubcommandLabel = "COMMAND",
        subcommands = {
                Read.class,
                Write.class,
                Test.class
        })
public class CLI implements Runnable
{
    @CommandLine.Spec
    CommandLine.Model.CommandSpec spec;

    @Override
    public void run()
    {
        throw new CommandLine.ParameterException(spec.commandLine(), "Missing required subcommand");
    }
}
