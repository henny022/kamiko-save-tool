package de.henny022.kamikosgt.cli;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.henny022.kamikosgt.model.CharacterData;
import de.henny022.kamikosgt.model.SaveFile;
import de.henny022.kamikosgt.util.SavefileIO;
import picocli.CommandLine;

import java.io.*;

/**
 * kamikoSaveEditor, Created by henny on 09.05.2020.
 */
@CommandLine.Command(name = "write")
public class Write implements Runnable
{
    @CommandLine.Option(names = {"-s", "--source"}, required = true)
    private File source;
    @CommandLine.Option(names = {"-t", "--target"}, required = true)
    private File target;
    @CommandLine.Option(names = {"-n", "--save"}, required = true)
    private int save;

    @Override
    public void run()
    {
        try
        {
            SaveFile saveFile = SavefileIO.read(target);
            CharacterData data = new CharacterData().loadJson(source);
            saveFile.replaceCharacterData(save, data);
            SavefileIO.write(target, saveFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
